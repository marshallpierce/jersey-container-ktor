package org.mpierce.jersey.ktor

import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.coroutines.Dispatchers
import org.glassfish.jersey.server.ResourceConfig
import org.slf4j.bridge.SLF4JBridgeHandler

/**
 * A runnable demo you can hit with curl, etc.
 */
object DemoMain {
    @JvmStatic
    fun main(args: Array<String>) {
        // Jersey uses JUL
        SLF4JBridgeHandler.removeHandlersForRootLogger()
        SLF4JBridgeHandler.install()

        val jerseyApp = ResourceConfig().apply {
            register(BasicResource::class.java)
            register(RootResource::class.java)

            property("jersey.config.server.tracing.type", "ALL")
            property("jersey.config.server.tracing.threshold", "VERBOSE")
        }

        embeddedServer(Netty, port = 11000) {
            routing {
                jerseyApp(jerseyApp, "/", Dispatchers.IO)

                post("/") {
                    call.respond("POST / (ktor)")
                }
                get("/ktor") {
                    call.respond("GET /ktor")
                }
            }
        }.start(wait = true)
    }
}
