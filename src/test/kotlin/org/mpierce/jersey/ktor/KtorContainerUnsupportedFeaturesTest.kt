package org.mpierce.jersey.ktor

import io.ktor.http.HttpMethod
import io.ktor.routing.routing
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import kotlinx.coroutines.Dispatchers
import org.glassfish.jersey.server.ContainerException
import org.glassfish.jersey.server.ResourceConfig
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.slf4j.bridge.SLF4JBridgeHandler
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.container.AsyncResponse
import javax.ws.rs.container.Suspended

class KtorContainerUnsupportedFeaturesTest {
    @Test
    internal fun asyncDoesntWork() {
        withTestApplication({
            routing {
                jerseyApp(
                    ResourceConfig().apply {
                        register(AsyncResource::class.java)
                    },
                    "",
                    Dispatchers.IO
                )
            }
        }) {
            assertThrows<ContainerException> {
                handleRequest(HttpMethod.Get, "/async") {}
            }
        }
    }

    companion object {
        @BeforeAll
        @JvmStatic
        fun setUp() {
            // Jersey uses JUL
            SLF4JBridgeHandler.removeHandlersForRootLogger()
            SLF4JBridgeHandler.install()
        }
    }
}

@Path("/async")
class AsyncResource {
    @GET
    fun asyncGet(@Suspended asyncResponse: AsyncResponse) {
        Thread {
            Thread.sleep(1000)
            asyncResponse.resume("done")
        }.start()
    }
}
