package org.mpierce.jersey.ktor

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.http.withCharset
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.testing.contentType
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import kotlinx.coroutines.Dispatchers
import org.glassfish.jersey.server.ResourceConfig
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.slf4j.bridge.SLF4JBridgeHandler
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import kotlin.test.assertEquals

internal abstract class KtorContainerPathTestBase {

    abstract val prefix: String

    private val prefixNoSlashes: String
        get() = prefix.removeSuffix("/").removePrefix("/")

    private val prefixLeadingSlashIfNotEmpty: String
        get() = if (prefixNoSlashes.isNotEmpty()) {
            "/$prefixNoSlashes"
        } else {
            ""
        }

    @Test
    internal fun canGetPlainTextResource() {
        withTestApplication({ jerseySetup(prefix) }) {
            with(handleRequest(HttpMethod.Get, "$prefixNoSlashes/basic")) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(ContentType.Text.Plain, response.contentType())
                assertEquals("GET basic", response.content!!)
            }
        }
    }

    @Test
    internal fun canGetPlainTextResourceTrailingSlash() {
        withTestApplication({ jerseySetup(prefix) }) {
            with(handleRequest(HttpMethod.Get, "$prefixNoSlashes/basic/")) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(ContentType.Text.Plain, response.contentType())
                assertEquals("GET basic", response.content!!)
            }
        }
    }

    @Test
    internal fun canGetPlainTextResourceLeadingSlash() {
        withTestApplication({ jerseySetup(prefix) }) {
            with(handleRequest(HttpMethod.Get, "$prefixLeadingSlashIfNotEmpty/basic")) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(ContentType.Text.Plain, response.contentType())
                assertEquals("GET basic", response.content!!)
            }
        }
    }

    @Test
    internal fun canGetPlainTextResourceLeadingSlashTrailingSlash() {
        withTestApplication({ jerseySetup(prefix) }) {
            with(handleRequest(HttpMethod.Get, "$prefixLeadingSlashIfNotEmpty/basic/")) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(ContentType.Text.Plain, response.contentType())
                assertEquals("GET basic", response.content!!)
            }
        }
    }

    @Test
    internal fun canGetPlainTextSubresource() {
        withTestApplication({ jerseySetup(prefix) }) {
            with(handleRequest(HttpMethod.Get, "$prefixNoSlashes/basic/subresource")) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(ContentType.Text.Plain, response.contentType())
                assertEquals("GET basic/subresource", response.content!!)
            }
        }
    }

    @Test
    internal fun canGetPlainTextRootResourceWithNoSlashes() {
        withTestApplication({ jerseySetup(prefix) }) {
            with(handleRequest(HttpMethod.Get, prefixNoSlashes)) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(ContentType.Text.Plain, response.contentType())
                assertEquals("GET root", response.content!!)
            }
        }
    }

    @Test
    internal fun canGetPlainTextRootResourceWithExactPrefix() {
        withTestApplication({ jerseySetup(prefix) }) {
            with(handleRequest(HttpMethod.Get, prefix)) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(ContentType.Text.Plain, response.contentType())
                assertEquals("GET root", response.content!!)
            }
        }
    }

    @Test
    internal fun canGetPlainTextRootResourceWithTrailingSlash() {
        withTestApplication({ jerseySetup(prefix) }) {
            with(handleRequest(HttpMethod.Get, "$prefixNoSlashes/")) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(ContentType.Text.Plain, response.contentType())
                assertEquals("GET root", response.content!!)
            }
        }
    }

    @Test
    internal fun canGetPlainTextRootResourceWithLeadingSlash() {
        withTestApplication({ jerseySetup(prefix) }) {
            with(handleRequest(HttpMethod.Get, prefixLeadingSlashIfNotEmpty)) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(ContentType.Text.Plain, response.contentType())
                assertEquals("GET root", response.content!!)
            }
        }
    }
    @Test
    internal fun canGetPlainTextRootResourceWithLeadingAndTrailingSlash() {
        withTestApplication({ jerseySetup(prefix) }) {
            with(handleRequest(HttpMethod.Get, "$prefixLeadingSlashIfNotEmpty/")) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(ContentType.Text.Plain, response.contentType())
                assertEquals("GET root", response.content!!)
            }
        }
    }

    @Test
    internal fun canGetPlainTextKtorEndpoint() {
        withTestApplication({ jerseySetup(prefix) }) {
            with(handleRequest(HttpMethod.Get, "/ktor")) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(ContentType.Text.Plain.withCharset(Charsets.UTF_8), response.contentType())
                assertEquals("GET ktor", response.content!!)
            }
        }
    }

    @Test
    internal fun canPostToPlainTextKtorEndpointWithSamePathAsJaxRsGet() {
        withTestApplication({ jerseySetup(prefix) }) {
            with(handleRequest(HttpMethod.Post, "$prefixNoSlashes/basic")) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(ContentType.Text.Plain.withCharset(Charsets.UTF_8), response.contentType())
                assertEquals("POST ktor", response.content!!)
            }
        }
    }

    @Test
    internal fun canHitKtorEndpointWithoutLeadingSlash() {
        withTestApplication({ jerseySetup(prefix) }) {
            with(handleRequest(HttpMethod.Get, "ktor")) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(ContentType.Text.Plain.withCharset(Charsets.UTF_8), response.contentType())
                assertEquals("GET ktor", response.content!!)
            }
        }
    }

    @Test
    internal fun canHitKtorEndpointWithLeadingSlash() {
        withTestApplication({ jerseySetup(prefix) }) {
            with(handleRequest(HttpMethod.Get, "/ktor")) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(ContentType.Text.Plain.withCharset(Charsets.UTF_8), response.contentType())
                assertEquals("GET ktor", response.content!!)
            }
        }
    }

    companion object {
        @BeforeAll
        @JvmStatic
        fun setUp() {
            // Jersey uses JUL
            SLF4JBridgeHandler.removeHandlersForRootLogger()
            SLF4JBridgeHandler.install()
        }
    }
}

internal class EmptyPrefixKtorContainerPathTest : KtorContainerPathTestBase() {
    override val prefix = ""
}

internal class SlashPrefixKtorContainerPathTest : KtorContainerPathTestBase() {
    override val prefix = "/"
}

internal class NoSlashPrefixKtorContainerPathTest : KtorContainerPathTestBase() {
    override val prefix = "no-slash"
}

internal class LeadingSlashPrefixKtorContainerPathTest : KtorContainerPathTestBase() {
    override val prefix = "/leading-slash"
}

internal class TrailingSlashPrefixKtorContainerPathTest : KtorContainerPathTestBase() {
    override val prefix = "trailing-slash/"
}

internal class BothSlashesPrefixKtorContainerPathTest : KtorContainerPathTestBase() {
    override val prefix = "/both-slashes/"
}

private fun Application.jerseySetup(prefix: String) {
    routing {
        jerseyApp(appForResources(BasicResource::class.java, RootResource::class.java), prefix, Dispatchers.IO)

        get("/ktor") {
            call.respond("GET ktor")
        }
        // same path as jersey GET
        post("${prefix.removePrefix("/").removeSuffix("/")}/basic") {
            call.respond("POST ktor")
        }
    }
}

private fun appForResources(vararg klasses: Class<*>): ResourceConfig = ResourceConfig().apply {
    klasses.forEach { register(it) }

    property("jersey.config.server.tracing.type", "ALL")
    property("jersey.config.server.tracing.threshold", "VERBOSE")
}

@Path("/")
class RootResource {
    @GET
    fun get(): String = "GET root"
}

@Path("basic")
@Produces(MediaType.TEXT_PLAIN)
class BasicResource {

    @GET
    fun get(): String = "GET basic"

    @GET
    @Path("subresource")
    fun subresource(): String = "GET basic/subresource"
}
