package org.mpierce.jersey.ktor

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpStatusCode
import io.ktor.request.httpMethod
import io.ktor.request.receiveStream
import io.ktor.request.uri
import io.ktor.response.ApplicationResponse
import io.ktor.response.header
import io.ktor.response.respond
import io.ktor.response.respondBytes
import io.ktor.routing.Routing
import io.ktor.routing.route
import io.ktor.util.pipeline.ContextDsl
import io.ktor.util.pipeline.PipelineContext
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.glassfish.jersey.internal.MapPropertiesDelegate
import org.glassfish.jersey.server.ApplicationHandler
import org.glassfish.jersey.server.ContainerException
import org.glassfish.jersey.server.ContainerRequest
import org.glassfish.jersey.server.ContainerResponse
import org.glassfish.jersey.server.ResourceConfig
import org.glassfish.jersey.server.spi.Container
import org.glassfish.jersey.server.spi.ContainerResponseWriter
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.OutputStream
import java.lang.RuntimeException
import java.net.URI
import java.security.Principal
import java.util.concurrent.TimeUnit
import javax.ws.rs.core.Application
import javax.ws.rs.core.SecurityContext

/**
 * Host a JAX-RS app in a Ktor app.
 */
@ContextDsl
fun Routing.jerseyApp(
    jaxRsApp: Application,
    /**
     * The URL path prefix to use for requests that should be served by `jaxRsApp`.
     *
     * This defines the ktor route used as well as the "base URI" provided to Jersey. Commonly this would be "/" if you're importing an existing JAX-RS app, but it could also be something like `/jersey/` if you wanted to partition those endpoints into a separate part of URL space.
     */
    pathPrefix: String,
    /**
     * The dispatcher that JAX-RS endpoints will be invoked on. Since they will use blocking I/O, the dispatcher should have many threads. Whatever size of thread pool was used for where the JAX-RS app used to be hosted should be used here.
     *
     * Example: `Executors.newFixedThreadPool(100).asCoroutineDispatcher()` if you need precise control, or `Dispatchers.IO` if your site is low traffic and the defaults are fine.
     */
    dispatcher: CoroutineDispatcher
) {
    val container = KtorContainer(jaxRsApp, pathPrefix, dispatcher)

    val noSlashesPath = pathPrefix.removePrefix("/").removeSuffix("/")

    // trailing slashes won't map to the no-slashes path, so have to handle those separately
    listOf(
        "$noSlashesPath/",
        "$noSlashesPath/{...}",
        "$noSlashesPath/{...}/",
    )
        .forEach { path ->
            route(path) {
                handle {
                    container.handleKtorRequest(this)
                }
            }
        }
}

internal class KtorContainer(
    jerseyApp: Application,
    private val pathPrefix: String,
    /**
     * The dispatcher to handle Jersey requests on
     */
    private val dispatcher: CoroutineDispatcher
) : Container {
    /**
     * Jersey wants the base URI to end in a /.
     * Since we normalize request uris to have a leading /, we do the same here as well.
     */
    private val baseUri = run {
        val withSuffix = if (pathPrefix.endsWith("/")) {
            pathPrefix
        } else {
            "$pathPrefix/"
        }

        val withSlashes = if (withSuffix.startsWith("/")) {
            withSuffix
        } else {
            "/$withSuffix"
        }

        URI(withSlashes)
    }

    private var appHandler = ApplicationHandler(jerseyApp)

    override fun getConfiguration(): ResourceConfig = appHandler.configuration

    override fun reload(): Unit = throw UnsupportedOperationException("Reload not supported")

    override fun reload(configuration: ResourceConfig?) = reload()

    override fun getApplicationHandler(): ApplicationHandler = appHandler
    suspend fun handleKtorRequest(pipelineContext: PipelineContext<Unit, ApplicationCall>) {
        pipelineContext.apply {
            withContext(dispatcher) {
                // when writing tests with `withTestApplication`, you can skip the leading slash in the path
                // and ktor will deal with it. However, jersey does not, so to make ktor tests behave the same,
                // insert a leading /. Also, this is just the path, despite the name.
                val uriWithSlash = if (call.request.uri.startsWith("/")) {
                    call.request.uri
                } else {
                    "/${call.request.uri}"
                }

                val jerseyReq = ContainerRequest(
                    baseUri,
                    URI(uriWithSlash),
                    call.request.httpMethod.value,
                    StubSecurityContext,
                    MapPropertiesDelegate(),
                    appHandler.configuration
                )

                jerseyReq.entityStream = call.receiveStream()

                call.request.headers.forEach { name, values ->
                    jerseyReq.headers(name, values)
                }

                jerseyReq.setWriter(KtorRequestWriter(call.response))

                appHandler.handle(jerseyReq)
            }
        }
    }
}

private object StubSecurityContext : SecurityContext {
    override fun isUserInRole(role: String?): Boolean = false

    override fun getAuthenticationScheme(): String? = null

    override fun getUserPrincipal(): Principal? = null

    override fun isSecure(): Boolean = false
}

private class KtorRequestWriter(
    private val response: ApplicationResponse
) : ContainerResponseWriter {
    @Volatile
    private var responseStatusSent = false

    @Volatile
    private var stream: BufferingOutputStream? = null

    override fun enableResponseBuffering(): Boolean = false

    override fun suspend(
        timeOut: Long,
        timeUnit: TimeUnit?,
        timeoutHandler: ContainerResponseWriter.TimeoutHandler?
    ): Boolean = TODO("Not yet implemented")

    override fun setSuspendTimeout(timeOut: Long, timeUnit: TimeUnit?): Unit = TODO("Not yet implemented")

    override fun writeResponseStatusAndHeaders(contentLength: Long, responseContext: ContainerResponse): OutputStream {

        responseContext.stringHeaders.entries
            .asSequence()
            // skip the ones ktor doesn't want us to set
            .filter { !HttpHeaders.isUnsafe(it.key) }
            .forEach { (name, values) ->
                values.forEach { value ->
                    response.header(name, value)
                }
            }

        val contentType = responseContext.stringHeaders.getFirst(HttpHeaders.ContentType)
            ?.let(ContentType.Companion::parse)

        // This won't do _exactly_ the right thing (a flush() won't really flush, for instance)
        return BufferingOutputStream { output ->
            responseStatusSent = true
            response.call.respondBytes(
                contentType,
                HttpStatusCode.fromValue(responseContext.statusInfo.statusCode)
            ) { output }
        }.also {
            stream = it
        }
    }

    override fun commit() {
        stream?.close()
    }

    override fun failure(error: Throwable) {
        try {
            if (!responseStatusSent) {
                runBlocking {
                    response.call.respond(HttpStatusCode.InternalServerError)
                }
            }
        } finally {
            commit()
            when (error) {
                is RuntimeException -> throw error
                else -> throw ContainerException(error)
            }
        }
    }
}

private class BufferingOutputStream(private val onClose: suspend (ByteArray) -> Unit) : OutputStream() {
    private val delegate = ByteArrayOutputStream()

    @Volatile
    private var closed = false
    override fun write(byte: Int) {
        delegate.write(byte)
    }

    override fun write(b: ByteArray, off: Int, len: Int) {
        delegate.write(b, off, len)
    }

    override fun close() {
        delegate.close()

        // only run callback once
        if (!closed) {
            synchronized(this) {
                if (!closed) {
                    closed = true
                    runBlocking {
                        try {
                            onClose(delegate.toByteArray())
                        } catch (e: Exception) {
                            throw IOException("Write on close failed", e)
                        }
                    }
                }
            }
        }
    }
}
