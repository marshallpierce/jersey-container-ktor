import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.util.Date

plugins {
    java
    `maven-publish`
    kotlin("jvm") version "1.4.21"
    id("org.jetbrains.dokka") version "1.4.20"
    id("org.jmailen.kotlinter") version "3.3.0"
    id("com.github.ben-manes.versions") version "0.36.0"
    id("com.jfrog.bintray") version "1.8.5"
    id("net.researchgate.release") version "2.8.1"
}

repositories {
    jcenter()
}

val deps by extra {
    mapOf(
        "jersey" to "2.30.1",
        "junit" to "5.7.0",
        "ktor" to "1.5.0",
        "slf4j" to "1.7.30"
    )
}

dependencies {
    implementation(kotlin("stdlib"))

    api("io.ktor", "ktor-server-core", deps["ktor"])

    api("org.glassfish.jersey.core", "jersey-server", deps["jersey"])

    testImplementation(kotlin("test-junit5"))
    testImplementation("org.junit.jupiter", "junit-jupiter-api", "${deps["junit"]}")
    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", "${deps["junit"]}")

    testImplementation("io.ktor", "ktor-server-test-host", "${deps["ktor"]}")
    testImplementation("io.ktor", "ktor-server-netty", "${deps["ktor"]}")

    testImplementation("org.glassfish.jersey.inject", "jersey-hk2", deps["jersey"])
    // jersey uses JUL because of course it does
    testImplementation("org.slf4j", "jul-to-slf4j", deps["slf4j"])

}

group = "org.mpierce.jersey.ktor"

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    withSourcesJar()
}

tasks {
    test {
        useJUnitPlatform()
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }

    register<Jar>("docJar") {
        from(project.tasks["dokkaHtml"])
        archiveClassifier.set("javadoc")
    }

    afterReleaseBuild {
        dependsOn(bintrayUpload)
    }
}

publishing {
    publications {
        register<MavenPublication>("bintray") {
            from(components["java"])
            artifact(tasks["docJar"])
        }
    }

    configure<com.jfrog.bintray.gradle.BintrayExtension> {
        user = rootProject.findProperty("bintrayUser")?.toString()
        key = rootProject.findProperty("bintrayApiKey")?.toString()
        setPublications("bintray")

        with(pkg) {
            repo = "maven"
            setLicenses("Copyfree")
            vcsUrl = "https://bitbucket.org/marshallpierce/jersey-container-ktor"
            name = "jersey-container-ktor"

            with(version) {
                name = project.version.toString()
                released = Date().toString()
                vcsTag = project.version.toString()
            }
        }
    }
}
