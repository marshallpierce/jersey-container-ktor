 [ ![Download](https://api.bintray.com/packages/marshallpierce/maven/jersey-container-ktor/images/download.svg) ](https://bintray.com/marshallpierce/maven/jersey-container-ktor/_latestVersion) 

# What is this?

A way to host JAX-RS resources in Jersey with Ktor as the [Jersey Container](https://eclipse-ee4j.github.io/jersey.github.io/documentation/latest/modules-and-dependencies.html#dependencies-table-org.glassfish.jersey.containers). In other words, Ktor is taking the place of Jetty, or Netty, or Grizzly, or any of the other Container implementations.

This allows you to migrate from a JAX-RS service to a Ktor service smoothly by running JAX-RS and Ktor endpoints side by side.

# Usage

Use your existing JAX-RS `Application`. If you're already using Jersey, you probably have some code already that constructs a `ResourceConfig` (Jersey's implementation of `Application`).

```kotlin
val jerseyApp = ResourceConfig().apply {
    register(SomeResource::class.java)
    register(AnotherResource::class.java)
    // however you choose to set up your JAX-RS application ...
}
```

You'll also need a dispatcher that your JAX-RS endpoints will run on. Since those endpoints are blocking, it will need to be a dispatcher backed by a thread pool. `Dispatchers.IO` can work if you are ok with that pool size, or specify your own if you know what pool size you want:

```
val dispatcher = Executors.newFixedThreadPool(100).asCoroutineDispatcher()
```

Finally, install the JAX-RS app into your Ktor app:

```kotlin
// start up your Ktor server however you like
val server = embeddedServer(Netty, port = 8080) {
    routing {
        jerseyApp(jerseyApp, "/", dispatcher)
    
        // use other route config as normal
        post("/stuff") {
            call.respond("A response from Ktor")
        }
        // ... other app setup
    }
}
```

# Limitations

- Output from JAX-RS endpoints is buffered before passing to Ktor.
- `SecurityContext` is not populated.
- JAX-RS's `AsyncResponse` is unsupported, but if you're running in Ktor, you have much better options available anyway.
- No reloading. Does anyone use this in practice?

All of these can likely be addressed if someone cares to implement them.
